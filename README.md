# Multi Module Codeception Test Runner

```
➜ ./module-runner
webproject-xyz/codeception-multi-module-test-runner 1.0.0

Usage:
  command [options] [arguments]

Options:
  -h, --help            Display this help message
  -q, --quiet           Do not output any message
  -V, --version         Display this application version
      --ansi            Force ANSI output
      --no-ansi         Disable ANSI output
  -n, --no-interaction  Do not ask any interactive question
  -v|vv|vvv, --verbose  Increase the verbosity of messages: 1 for normal output, 2 for more verbose output and 3 for debug

Available commands:
  help                        Displays help for a command
  list                        Lists commands
 codeception
  codeception:modules:detect  List modules in mvc structure
  codeception:modules:init    Bootstrap Codeception in every module and add sample tests
  codeception:modules:run     Run single module


```

### Example outputs
##### detect, no filter
```
➜ ./module-runner codeception:modules:detect -p tests/_data/app -e Application

Detect modules:
===============

Scan Module directory
---------------------

 6/6 [▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓] 100%

 ------------- ----------------- ------------------ 
  ModuleName    Has Codeception   Has Bootstrapped  
 ------------- ----------------- ------------------ 
  admin         ok                ok                
  application   NO!               NO!               
  core          ok                ok                
  error         ok                ok                
  no-codecept   NO!               NO!               
  skip          NO!               NO!               
 ------------- ----------------- ------------------ 

```

##### detect, filter "application" and "skip"
```
➜ ./module-runner codeception:modules:detect -p tests/_data/app -e Application -e skip

Detect modules:
===============

Scan Module directory
---------------------

 6/6 [▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓] 100%

 ------------- ----------------- ------------------ 
  ModuleName    Has Codeception   Has Bootstrapped  
 ------------- ----------------- ------------------ 
  admin         ok                ok                
  application   NO!               NO!               
  core          ok                ok                
  error         ok                ok                
  no-codecept   NO!               NO!               
 ------------- ----------------- ------------------ 

```

#### init command: 

```
➜ ./module-runner codeception:modules:init -h                        
Description:
  Bootstrap Codeception in every module and add sample tests

Usage:
  codeception:modules:init [options] [--] [<moduleNames>...]

Arguments:
  moduleNames                                                      List of module names to run. default: all

Options:
  -f, --force[=FORCE]                                              Force bootstrap without codeception.yml [default: false]
  -p, --path=PATH                                                  path to app
  -e, --exclude[=EXCLUDE]                                          module names to skip (multiple values allowed)
  -s, --hide-progress[=HIDE-PROGRESS]                              hide output of progress bar [default: false]
  -h, --help                                                       Display this help message
  -q, --quiet                                                      Do not output any message
  -V, --version                                                    Display this application version
      --ansi                                                       Force ANSI output
      --no-ansi                                                    Disable ANSI output
  -n, --no-interaction                                             Do not ask any interactive question
  -so, --showCodeceptionOutput[=SHOWCODECEPTIONOUTPUT]             Show codeception console output [default: false]
  -sor, --showCodeceptionErrorOutput[=SHOWCODECEPTIONERROROUTPUT]  Show codeception console output only for failed modules [default: false]
  -v|vv|vvv, --verbose                                             Increase the verbosity of messages: 1 for normal output, 2 for more verbose output and 3 for debug
```
##### init: example
```
➜ ./module-runner codeception:modules:init -p tests/_data/app

Bootstrap Codeception in modules:
=================================

Scan Module directory
---------------------

 6/6 [▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓] 100%

 No module name given. Select one or run all? [Run all]:
  [0] Select one
  [1] Run all
 > 0

 Select module to run:
  [0] admin
  [1] application
  [2] core
  [3] error
  [4] no-codecept
  [5] skip
 > 5

Bootstrapping modules:
----------------------

 1/1 [▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓] 100%

 ------------ -------- -------------------- 
  Name         Result   Message             
 ------------ -------- -------------------- 
  Skip         ok       Bootstrap finished  
 ------------ -------- -------------------- 

```
###### init: run command for specific module name "skip" (missing codeception.yml in "skip" module)
```
➜ ./module-runner codeception:modules:init -p tests/_data/app skip

Bootstrap Codeception in modules:
=================================

Scan Module directory
---------------------

 6/6 [▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓] 100%

Bootstrapping modules:
----------------------

 1/1 [▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓] 100%

 ------------ -------- --------------------------------- 
  Name         Result   Message                          
 ------------ -------- --------------------------------- 
  Skip         error!   Missing codeception.yml in skip  
 ------------ -------- --------------------------------- 

```

```
➜ ./module-runner codeception:modules:init -p tests/_data/app skip -f

Bootstrap Codeception in modules:
=================================

Scan Module directory
---------------------

 6/6 [▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓] 100%

Bootstrapping modules:
----------------------

 1/1 [▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓] 100%

 ------------ -------- -------------------- 
  Name         Result   Message             
 ------------ -------- -------------------- 
  Skip         ok       Bootstrap finished  
 ------------ -------- -------------------- 

```

`➜ ./module-runner codeception:modules:run -p tests/_data/app -e application -e skip`
```
multi-module-codeception-test-runner on  master [!+?] via 🐘 v7.3.5 on 🐳 v18.09.1 
➜ ./module-runner codeception:modules:run -p tests/_data/app -e application -e skip

Run Codeception for modules:
============================

Scan Module directory
---------------------

 8/8 [▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓] 100%

 No module name given. Select one or run all? [Run all]:
  [0] Select one
  [1] Run all
 > 1

Executing modules:
------------------

 4/4 [▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓] 100%

 ------------- ------- ------------ -------- ---------- ------- ---------- -------- 
  ModuleName    Tests   Assertions   Errors   Failures   Time    Memory     Result  
 ------------- ------- ------------ -------- ---------- ------- ---------- -------- 
  Error         1       0            1        0          75 ms   10.00 MB   error!  
  Core          1       4            0        0          74 ms   10.00 MB   ok      
  Admin         2       2            0        2          76 ms   10.00 MB   error!  
  No-codecept   0       0            0        0                             error!  
 ------------- ------- ------------ -------- ---------- ------- ---------- -------- 

multi-module-codeception-test-runner on  master [!+?] via 🐘 v7.3.5 on 🐳 v18.09.1 took 8s 
 
```

##### Run single module "core"
```
multi-module-codeception-test-runner on  master [!+?] via 🐘 v7.3.5 on 🐳 v18.09.1 
➜ ./module-runner codeception:modules:run core -p tests/_data/app

Run Codeception for modules:
============================

Scan Module directory
---------------------

 8/8 [▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓] 100%

Executing modules:
------------------

 1/1 [▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓] 100%

 ------------ ------- ------------ -------- ---------- ------- ---------- -------- 
  ModuleName   Tests   Assertions   Errors   Failures   Time    Memory     Result  
 ------------ ------- ------------ -------- ---------- ------- ---------- -------- 
  Core         1       4            0        0          74 ms   10.00 MB   ok      
 ------------ ------- ------------ -------- ---------- ------- ---------- -------- 
 ```

##### Run all no progress bars

```
➜ ./module-runner codeception:modules:run --hide-progress -p tests/_data/app  

Run Codeception for modules:
============================

Scan Module directory
---------------------

 No module name given. Select one or run all? [Run all]:
  [0] Select one
  [1] Run all
 > 

Executing modules:
------------------

 ------------- ------- ------------ -------- ---------- ------- ---------- -------- 
  ModuleName    Tests   Assertions   Errors   Failures   Time    Memory     Result  
 ------------- ------- ------------ -------- ---------- ------- ---------- -------- 
  Error         1       0            1        0          83 ms   10.00 MB   error!  
  Skip          0       0            0        0                             error!  
  Core          1       4            0        0          74 ms   10.00 MB   ok      
  Admin         2       2            0        2          75 ms   10.00 MB   error!  
  Application   0       0            0        0                             error!  
  No-codecept   0       0            0        0                             error!  
 ------------- ------- ------------ -------- ---------- ------- ---------- -------- 

```

##### non interactive, all modules, no progress

` ./module-runner codeception:modules:run -s -p tests/_data/app -n `
```
➜ ./module-runner codeception:modules:run -s -p tests/_data/app -n 

Run Codeception for modules:
============================

Scan Module directory
---------------------

Executing modules:
------------------

 ------------- ------- ------------ -------- ---------- ------- ---------- -------- 
  ModuleName    Tests   Assertions   Errors   Failures   Time    Memory     Result  
 ------------- ------- ------------ -------- ---------- ------- ---------- -------- 
  Error         1       0            1        0          74 ms   10.00 MB   error!  
  Skip          0       0            0        0                             error!  
  Core          1       4            0        0          73 ms   10.00 MB   ok      
  Admin         2       2            0        2          74 ms   10.00 MB   error!  
  Application   0       0            0        0                             error!  
  No-codecept   0       0            0        0                             error!  
 ------------- ------- ------------ -------- ---------- ------- ---------- -------- 


```

