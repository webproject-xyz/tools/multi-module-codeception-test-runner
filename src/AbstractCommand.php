<?php
declare(strict_types=1);

namespace Webproject\Console;

use Psr\Log\LogLevel;
use Symfony\Component\Console\Command\Command as SymfonyCommand;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Logger\ConsoleLogger;
use Symfony\Component\Console\Output\OutputInterface;
use Webproject\Console\Dependency\Logger;

/**
 * Class Command
 */
abstract class AbstractCommand extends SymfonyCommand
{
    public const APP_VERSION = '1.0.0';
    public const RS_OK       = '<fg=green>ok</>';
    public const RS_NO_TESTS = '<fg=yellow>no test</>';
    public const RS_ERROR    = '<fg=red>error!</>';

    /** @var int */
    protected $statusCode = 0;

    /**
     * @param InputInterface  $input
     * @param OutputInterface $output
     *
     * @return null|int|void
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $myDependency = new Logger($this->getLogger($output));
        $myDependency->doLogInput($input);
    }

    /**
     * @param OutputInterface $output
     *
     * @return ConsoleLogger
     */
    protected function getLogger(OutputInterface $output): ConsoleLogger
    {
        $formatLevelMap = [
            LogLevel::CRITICAL => ConsoleLogger::ERROR,
            LogLevel::DEBUG    => ConsoleLogger::INFO,
        ];

        $verbosityLevelMap = [
            LogLevel::NOTICE => OutputInterface::VERBOSITY_NORMAL,
            LogLevel::INFO   => OutputInterface::VERBOSITY_NORMAL,
        ];

        return new ConsoleLogger($output, $verbosityLevelMap, $formatLevelMap);
    }
}
