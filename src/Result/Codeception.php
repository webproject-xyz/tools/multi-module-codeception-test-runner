<?php
declare(strict_types=1);

namespace Webproject\Console\Result;

/**
 * Class Result
 */
class Codeception
{
    /** @var string */
    protected $output;
    /** @var string */
    protected $time;
    /** @var string */
    protected $memory;
    /** @var int */
    protected $tests;
    /** @var int */
    protected $assertions;
    /** @var int */
    protected $errors;
    /** @var int */
    protected $failures;
    /** @var bool */
    protected $succsess = false;
    /** @var int */
    protected $code;
    /** @var bool */
    protected $noTests = false;

    /**
     * Get Output
     *
     * @return string
     */
    public function getOutput(): string
    {
        return $this->output ?? '';
    }

    /**
     * Set Output
     *
     * @param string $output
     *
     * @return static
     */
    public function setOutput(string $output): self
    {
        $this->output = $output;

        return $this;
    }

    /**
     * Get Time
     *
     * @return string
     */
    public function getTime(): string
    {
        return $this->time ?? '';
    }

    /**
     * Set Time
     *
     * @param string $time
     *
     * @return static
     */
    public function setTime(string $time): self
    {
        $this->time = $time;

        return $this;
    }

    /**
     * Get Memory
     *
     * @return string
     */
    public function getMemory(): string
    {
        return $this->memory ?? '';
    }

    /**
     * Set Memory
     *
     * @param string $memory
     *
     * @return static
     */
    public function setMemory(string $memory): self
    {
        $this->memory = $memory;

        return $this;
    }

    /**
     * Get Tests
     *
     * @return int
     */
    public function getTests(): int
    {
        return $this->tests ?? 0;
    }

    /**
     * Set Tests
     *
     * @param int $tests
     *
     * @return static
     */
    public function setTests(int $tests): self
    {
        $this->tests = $tests;

        return $this;
    }

    /**
     * Get Assertions
     *
     * @return int
     */
    public function getAssertions(): int
    {
        return $this->assertions ?? 0;
    }

    /**
     * Set Assertions
     *
     * @param int $assertions
     *
     * @return static
     */
    public function setAssertions(int $assertions): self
    {
        $this->assertions = $assertions;

        return $this;
    }

    /**
     * Get Errors
     *
     * @return int
     */
    public function getErrors(): int
    {
        return $this->errors ?? 0;
    }

    /**
     * Set Errors
     *
     * @param int $errors
     *
     * @return static
     */
    public function setErrors(int $errors): self
    {
        $this->errors = $errors;

        return $this;
    }

    /**
     * Get Failures
     *
     * @return int
     */
    public function getFailures(): int
    {
        return $this->failures ?? 0;
    }

    /**
     * Set Failures
     *
     * @param int $failures
     *
     * @return static
     */
    public function setFailures(int $failures): self
    {
        $this->failures = $failures;

        return $this;
    }

    /**
     * Get Succsess
     *
     * @return bool
     */
    public function isSuccsess(): bool
    {
        return $this->succsess;
    }

    /**
     * Set Succsess
     *
     * @param bool $succsess
     *
     * @return static
     */
    public function setSuccsess(bool $succsess): self
    {
        $this->succsess = $succsess;

        return $this;
    }

    /**
     * Get Code
     *
     * @return int
     */
    public function getCode(): int
    {
        return $this->code ?? 0;
    }

    /**
     * Set Code
     *
     * @param int $code
     *
     * @return static
     */
    public function setCode(int $code): self
    {
        $this->code = $code;

        return $this;
    }

    /**
     * Has no tests executed
     *
     * @return bool
     */
    public function hasNoTests(): bool
    {
        return true === $this->noTests;
    }

    /**
     * Set NoTests
     *
     * @param bool $noTests
     *
     * @return static
     */
    public function setNoTests(bool $noTests): self
    {
        $this->noTests = $noTests;

        return $this;
    }
}
