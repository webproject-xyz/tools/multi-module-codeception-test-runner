<?php
declare(strict_types=1);

namespace Webproject\Console\Parser\Codeception;

use Symfony\Component\Process\Process;
use Webproject\Console\Result\Codeception as Result;

/**
 * Class Output
 */
class Output
{
    /** @var Result */
    protected $result;

    public function __construct()
    {
        $this->result = new Result();
    }

    /**
     * @param Process $process
     *
     * @return static
     */
    public function parse(Process $process): self
    {
        $processOutput = $process->getOutput();
        $processLines  = \explode("\n", $processOutput);
        foreach ($processLines as $key => $processLine) {
            if (empty($processLine)) {
                unset($processLines[$key]);
            }
        }

        $resultObject = $this->result;

        // find time
        if (\preg_match('#Time: (\d* \w*)#m', $process->getOutput(), $matches)) {
            $resultObject->setTime($matches[1]);
        }

        // find memory
        if (\preg_match('#Memory: (\d*.* \w*)#m', $process->getOutput(), $matches)) {
            $resultObject->setMemory($matches[1]);
        }

        // find result (OK)
        if (\preg_match('#OK \((\d*) test[s|]?\, (\d*) assertion[s]?\)#m', $process->getOutput(), $matches)) {
            $resultObject->setSuccsess(true);
            $resultObject->setTests((int) $matches[1]);
            $resultObject->setAssertions((int) $matches[2]);
        }

        if (\preg_match('#Tests: (\d*)#m', $process->getOutput(), $matches)) {
            $resultObject->setTests((int) $matches[1]);
        }

        if (\preg_match('#Assertions: (\d*)#m', $process->getOutput(), $matches)) {
            $resultObject->setAssertions((int) $matches[1]);
        }

        if (\preg_match('#Failures: (\d*)#m', $process->getOutput(), $matches)) {
            $resultObject->setFailures((int) $matches[1]);
        }

        if (\preg_match('#Errors: (\d*)#m', $process->getOutput(), $matches)) {
            $resultObject->setErrors((int) $matches[1]);
        }

        if (\preg_match('#No tests executed!#m', $process->getOutput(), $matches)) {
            $resultObject->setNoTests(true);
        }

        $resultObject->setOutput(\implode("\n", $processLines));

        $this->result = $resultObject;

        return $this;
    }

    /**
     * Get Result
     *
     * @return Result
     */
    public function getResult(): Result
    {
        return $this->result;
    }
}
