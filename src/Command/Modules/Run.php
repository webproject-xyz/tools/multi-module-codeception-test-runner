<?php
declare(strict_types=1);

namespace Webproject\Console\Command\Modules;

use Symfony\Component\Console\Exception\LogicException;
use Symfony\Component\Console\Helper\Table;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;
use Symfony\Component\Process\Process;
use Webproject\Console\Parser\Codeception\Output as OutputParser;
use Webproject\Console\Result\Codeception as Result;

/**
 * Class RunModule
 */
class Run extends Detect
{
    /** @var Result[] */
    protected $results;

    protected function configure(): void
    {
        $this
            ->setName('codeception:modules:run')
            ->setDescription(
                'Run single module'
            )
            ->addArgument(
                'moduleNames',
                InputArgument::OPTIONAL | InputArgument::IS_ARRAY,
                'list of module names to run. default: all',
                []
            )
            ->addOption(
                'bootstrap',
                'b',
                InputOption::VALUE_OPTIONAL,
                'Run <info>codecept bootstrap</info>',
                false
            )
            ->addOption(
                'showCodeceptionOutput',
                'so',
                InputOption::VALUE_OPTIONAL,
                'Show codeception <info>console output</info>',
                false
            )
            ->addOption(
                'showCodeceptionErrorOutput',
                'sor',
                InputOption::VALUE_OPTIONAL,
                'Show codeception <info>console output</info> only for failed modules',
                false
            );
    }

    /**
     * @param InputInterface  $input
     * @param OutputInterface $output
     *
     * @return null|int|void
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $cli = new SymfonyStyle($input, $output);
        $cli->title('Run Codeception for modules:');

        $this->scanForModules($input, $cli);

        $moduleNames = $input->getArgument('moduleNames');

        if (0 === \count($moduleNames)) {
            $answerSelection = $cli->choice(
                'No module name given. Select one or run all?',
                ['Select one', 'Run all'],
                'Run all'
            );

            if ('Run all' === $answerSelection) {
                $moduleNames = \array_keys($this->modules);
            } else {
                $answer      = $cli->choice('Select module to run', \array_keys($this->modules));
                $moduleNames = [$answer];
            }
        }

        $cli->section('Executing modules:');

        if ($this->showProgressBar($input)) {
            $cli->progressStart(\count($moduleNames));
        }

        $table = $this->buildOutputTable($output);

        foreach ($moduleNames as $moduleName) {
            $this->runCodeceptionOnModule($input, $output, $moduleName, $table);
            if ($this->showProgressBar($input)) {
                $cli->progressAdvance();
            }
        }

        if ($this->showProgressBar($input)) {
            $cli->progressFinish();
        }

        $table->render();

        return $this->statusCode;
    }

    /**
     * @param Process $process
     *
     * @return Result
     */
    protected function parseProcessOutput(Process $process): Result
    {
        return (new OutputParser())
            ->parse($process)
            ->getResult();
    }

    /**
     * @param InputInterface  $input
     * @param OutputInterface $output
     * @param string          $moduleName
     * @param Table           $table
     */
    protected function runCodeceptionOnModule(
        InputInterface $input,
        OutputInterface $output,
        string $moduleName,
        Table $table
    ): void {
        if (! \array_key_exists($moduleName, $this->modules)) {
            throw new LogicException('Module with name "' . $moduleName . '" not found');
        }

        $moduleDirectory = $this->modules[$moduleName];

        \putenv('SYMFONY_DEPRECATIONS_HELPER=weak');

        $process = new Process(
            [
                \getcwd() . '/vendor/bin/codecept',
                'run',
                'unit',
                '--xml=junit.' . $moduleName . '.xml',
                '--no-ansi',
                '--no-interaction',
            ],
            $moduleDirectory->getPath(),
            [
                'SYMFONY_DEPRECATIONS_HELPER' => 'weak',
            ]
        );

        $process->run();

        if ((int) $process->getExitCode() > 0) {
            $this->statusCode = $process->getExitCode();
        }

        $result          = $this->parseProcessOutput($process);
        $this->results[] = $result;

        $message = $result->isSuccsess()
            ? self::RS_OK
            : self::RS_ERROR;

        if ($result->hasNoTests()) {
            $message = self::RS_NO_TESTS;
        }

        $table
            ->addRow([
                \ucfirst($moduleDirectory->getModuleName()),
                $result->getTests(),
                $result->getAssertions(),
                $result->getErrors(),
                $result->getFailures(),
                $result->getTime(),
                $result->getMemory(),
                $message,
            ]);

        if (false !== $input->getOption('showCodeceptionOutput')
            || (! $process->isSuccessful() && false !== $input->getOption('showCodeceptionErrorOutput'))
        ) {
            $output->writeln($process->getOutput());
        }
    }

    /**
     * @param OutputInterface $output
     *
     * @return Table
     */
    protected function buildOutputTable(OutputInterface $output): Table
    {
        $table = new Table($output);
        $table
            ->setColumnWidths([
                10,
                5,
                10,
                6,
                8,
                6,
                6,
                6,
            ])
            ->setHeaders([
                'Name',
                'Tests',
                'Assertions',
                'Errors',
                'Failures',
                'Time',
                'Memory',
                'Result',
            ])
            ->setStyle('symfony-style-guide');

        return $table;
    }
}
