<?php
declare(strict_types=1);

namespace Webproject\Console\Command\Modules;

use Symfony\Component\Console\Exception\RuntimeException;
use Symfony\Component\Console\Helper\Table;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;
use Symfony\Component\Finder\Finder;
use Symfony\Component\Finder\Iterator\PathFilterIterator;
use Symfony\Component\Finder\SplFileInfo;
use Symfony\Component\Process\Exception\LogicException;
use Symfony\Component\Process\Exception\RuntimeException as ProcessRuntimeException;
use Webproject\Console\AbstractCommand;
use Webproject\Console\Directory\Codeception as CodeceptionDirectory;

/**
 * Class DetectModules
 */
class Detect extends AbstractCommand
{
    /** @var array|CodeceptionDirectory[] */
    protected $modules = [];

    /**
     * @param null|string $name
     */
    public function __construct(?string $name = null)
    {
        parent::__construct($name);
        $this
            ->addOption(
                'path',
                'p',
                InputOption::VALUE_REQUIRED,
                'path to app'
            )
            ->addOption(
                'exclude',
                'e',
                InputOption::VALUE_OPTIONAL | InputOption::VALUE_IS_ARRAY,
                'module names to skip',
                []
            )
            ->addOption(
                'hide-progress',
                's',
                InputOption::VALUE_OPTIONAL,
                'hide output of progress bar',
                false
            );
    }

    protected function configure(): void
    {
        $this
            ->setName('codeception:modules:detect')
            ->setDescription(
                'List modules in mvc structure'
            );
    }

    /**
     * @param InputInterface  $input
     * @param OutputInterface $output
     *
     * @return null|int|void
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $cli = new SymfonyStyle($input, $output);
        $cli->title('Detect modules:');

        $this->scanForModules($input, $cli);

        $table = $this->buildOutputTable($output);

        // sort by name
        \ksort($this->modules, \SORT_NATURAL);

        foreach ($this->modules as $codeception) {
            $hasYml = $codeception->hasCodeceptionFile()
                ? self::RS_OK
                : '<fg=red>NO!</>';

            $hasBootstrapped = $codeception->hasBootstrapped()
                ? self::RS_OK
                : '<fg=red>NO!</>';

            $table->addRow([
                $codeception->getModuleName(),
                $hasYml,
                $hasBootstrapped,
            ]);
        }

        $table->render();

        return $this->statusCode;
    }

    /**
     * @param InputInterface $input
     *
     * @throws RuntimeException
     *
     * @return PathFilterIterator|SplFileInfo[]
     */
    protected function getDirectoryIterator(InputInterface $input): PathFilterIterator
    {
        $path       = \getcwd() ?: __DIR__ . '/../../';
        $modulePath = \realpath((string) $input->getOption('path'));

        if (0 === \substr_count($modulePath, $path)) {
            throw new LogicException('outside scope');
        }

        $finder = new Finder();
        $finder
            ->directories()
            ->in($modulePath)
            ->depth(1)
            ->path(['modules/', 'module/', 'packages/'])
            ->notName(['_*', 'tests', 'src']);

        if (! $finder->hasResults()) {
            throw new ProcessRuntimeException('no modules found in path');
        }

        return $finder->getIterator();
    }

    /**
     * @param InputInterface $input
     * @param SymfonyStyle   $cli
     */
    protected function scanForModules(InputInterface $input, SymfonyStyle $cli): void
    {
        $cli->section('Scan Module directory');

        $iterator = $this->getDirectoryIterator($input);

        if ($this->showProgressBar($input)) {
            $cli->progressStart(\iterator_count($iterator));
        }

        foreach ($iterator as $moduleDirectory) {
            if (! $moduleDirectory->isDir()) {
                continue;
            }

            $name = $moduleDirectory->getBasename();
            if (\in_array($name, (array) $input->getOption('exclude'), true)) {
                if ($this->showProgressBar($input)) {
                    $cli->progressAdvance();
                }
                continue;
            }

            $codeception = new CodeceptionDirectory($name, $moduleDirectory);

            $this->modules[$codeception->getModuleName()] = $codeception;
            if ($this->showProgressBar($input)) {
                $cli->progressAdvance();
            }
        }

        \ksort($this->modules, \SORT_NATURAL);

        if ($this->showProgressBar($input)) {
            $cli->progressFinish();
        }
    }

    /**
     * @param InputInterface $input
     *
     * @return bool
     */
    protected function showProgressBar(InputInterface $input): bool
    {
        return false === $input->getOption('hide-progress');
    }

    /**
     * @param OutputInterface $output
     *
     * @return Table
     */
    protected function buildOutputTable(OutputInterface $output): Table
    {
        $table = new Table($output);
        $table
            ->setHeaders([
                'ModuleName',
                'Has Codeception',
                'Has Bootstrapped',
            ])
            ->setStyle('symfony-style-guide');

        return $table;
    }
}
