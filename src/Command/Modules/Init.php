<?php
declare(strict_types=1);

namespace Webproject\Console\Command\Modules;

use Symfony\Component\Console\Exception\LogicException;
use Symfony\Component\Console\Helper\Table;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;
use Symfony\Component\Process\Process;

/**
 * Class Init
 */
class Init extends Detect
{
    protected function configure(): void
    {
        $this
            ->setName('codeception:modules:init')
            ->setDescription(
                'Bootstrap Codeception in every module and add sample tests'
            )
            ->addArgument(
                'moduleNames',
                InputArgument::OPTIONAL | InputArgument::IS_ARRAY,
                'List of module names to run. default: all',
                []
            )
            ->addOption(
                'force',
                'f',
                InputOption::VALUE_OPTIONAL,
                'Force bootstrap without codeception.yml',
                false
            )
            ->addOption(
                'showCodeceptionOutput',
                'so',
                InputOption::VALUE_OPTIONAL,
                'Show codeception <info>console output</info>',
                false
            )
            ->addOption(
                'showCodeceptionErrorOutput',
                'sor',
                InputOption::VALUE_OPTIONAL,
                'Show codeception <info>console output</info> only for failed modules',
                false
            );
    }

    /**
     * @param InputInterface  $input
     * @param OutputInterface $output
     *
     * @return null|int|void
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $cli = new SymfonyStyle($input, $output);
        $cli->title('Bootstrap Codeception in modules:');

        $this->scanForModules($input, $cli);

        $table = new Table($output);
        $table
            ->setColumnWidths([
                10,
                5,
                10,
                6,
                8,
                6,
                6,
                6,
            ])
            ->setHeaders([
                'Name',
                'Result',
                'Message',
            ])
            ->setStyle('symfony-style-guide');

        $moduleNames = $input->getArgument('moduleNames');

        if (0 === \count($moduleNames)) {
            $answerSelection = $cli->choice(
                'No module name given. Select one or run all?',
                ['Select one', 'Run all'],
                'Run all'
            );

            if ('Run all' === $answerSelection) {
                $moduleNames = \array_keys($this->modules);
            } else {
                $answer      = $cli->choice('Select module to run', \array_keys($this->modules));
                $moduleNames = [$answer];
            }
        }

        $cli->section('Bootstrapping modules:');

        if ($this->showProgressBar($input)) {
            $cli->progressStart(\count($moduleNames));
        }

        foreach ($moduleNames as $moduleName) {
            $this->runBootstrapModule($input, $output, $moduleName, $table);
            if ($this->showProgressBar($input)) {
                $cli->progressAdvance();
            }
        }

        if ($this->showProgressBar($input)) {
            $cli->progressFinish();
        }

        $table->render();

        return $this->statusCode;
    }

    /**
     * @param InputInterface  $input
     * @param OutputInterface $output
     * @param string          $moduleName
     * @param Table           $table
     */
    protected function runBootstrapModule(
        InputInterface $input,
        OutputInterface $output,
        string $moduleName,
        Table $table
    ): void {
        if (! \array_key_exists($moduleName, $this->modules)) {
            throw new LogicException('Module with name "' . $moduleName . '" not found');
        }

        $moduleDirectory = $this->modules[$moduleName];

        $result  = self::RS_OK;
        $message = self::RS_OK;

        if (! $moduleDirectory->hasCodeceptionFile()) {
            // error
            $result  = self::RS_ERROR;
            $message = 'Missing codeception.yml in ' . $moduleName;
        }

        if (! $moduleDirectory->hasBootstrapped()
            && ($moduleDirectory->hasCodeceptionFile() || false !== $input->getOption('force'))
        ) {
            // cool
            $process = new Process(
                [
                    \getcwd() . '/vendor/bin/codecept',
                    'bootstrap',
                    '--no-ansi',
                    '--no-interaction',
                ],
                $moduleDirectory->getPath(),
                [
                    'SYMFONY_DEPRECATIONS_HELPER' => 'weak',
                ]
            );

            $process->run();

            if ((int) $process->getExitCode() > 0) {
                $this->statusCode = $process->getExitCode();
            }

            if ($process->isSuccessful()) {
                $result  = self::RS_OK;
                $message = 'Bootstrap finished';
            } else {
                $result  = self::RS_ERROR;
                $message = $process->getExitCodeText();
            }
        }

        $table
            ->addRow([
                \ucfirst($moduleDirectory->getModuleName()),
                $result,
                $message,
            ]);

        if (false !== $input->getOption('showCodeceptionOutput')
            || (isset($process) && ! $process->isSuccessful() && false !== $input->getOption('showCodeceptionErrorOutput'))
        ) {
            $output->writeln($process->getOutput());
        }
    }
}
