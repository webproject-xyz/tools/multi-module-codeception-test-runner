<?php
declare(strict_types=1);

namespace Webproject\Console\Dependency;

use Psr\Log\LoggerInterface;
use Symfony\Component\Console\Input\InputInterface;

/**
 * Class Logger
 */
class Logger
{
    /** @var LoggerInterface */
    protected $logger;

    /**
     * @param LoggerInterface $logger
     */
    public function __construct(LoggerInterface $logger)
    {
        $this->logger = $logger;
    }

    public function doLogInput(InputInterface $input)
    {
        $this->logger->info(
            'Command: ' . \implode(' ', $input->getArguments())
        );
    }
}
