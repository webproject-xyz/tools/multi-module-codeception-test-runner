<?php
declare(strict_types=1);

namespace Webproject\Console\Directory;

use JsonSerializable;
use SplFileInfo;
use Symfony\Component\Finder\SplFileInfo as DirectoryIterator;

/**
 * Class Codeception
 */
class Codeception implements JsonSerializable
{
    /** @var string */
    private $moduleName;

    /** @var DirectoryIterator */
    private $directory;

    /**
     * Get ModuleName
     *
     * @return string
     */
    public function getModuleName(): string
    {
        return $this->moduleName;
    }

    /**
     * @param string            $moduleName
     * @param DirectoryIterator $directory
     */
    public function __construct(string $moduleName, DirectoryIterator $directory)
    {
        $this->moduleName = $moduleName;
        $this->directory  = $directory;
    }

    /**
     * @return bool
     */
    public function hasCodeceptionFile(): bool
    {
        return (new SplFileInfo($this->directory->getRealPath() . \DIRECTORY_SEPARATOR . 'codeception.yml'))
            ->isFile();
    }

    /**
     * @return bool
     */
    public function hasBootstrapped(): bool
    {
        return (new SplFileInfo($this->directory->getRealPath() . \DIRECTORY_SEPARATOR . '/tests/_support/'))
            ->isDir();
    }

    /**
     * Specify data which should be serialized to JSON
     *
     * @link  https://php.net/manual/en/jsonserializable.jsonserialize.php
     *
     * @return mixed data which can be serialized by <b>json_encode</b>,
     *               which is a value of any type other than a resource.
     *
     * @since 5.4.0
     */
    public function jsonSerialize()
    {
        return [
            'name'        => $this->getModuleName(),
            'codeception' => [
                'hasFile'         => $this->hasCodeceptionFile(),
                'hasBootstrapped' => $this->hasBootstrapped(),
            ],
        ];
    }

    /**
     * Get Directory
     *
     * @return DirectoryIterator
     */
    public function getDirectory(): DirectoryIterator
    {
        return $this->directory;
    }

    /**
     * @return string
     */
    public function getPath(): string
    {
        return $this->getDirectory()->getRealPath();
    }
}
