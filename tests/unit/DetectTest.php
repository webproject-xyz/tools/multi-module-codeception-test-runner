<?php
declare(strict_types=1);

namespace Webproject\Console\Tests\Unit;

use Codeception\Test\Unit;
use Symfony\Component\Console\Application;
use Symfony\Component\Console\Tester\CommandTester;
use Webproject\Console\Command\Modules\Detect;

use function codecept_data_dir;

/**
 * Class DetectTest
 */
class DetectTest extends Unit
{
    public function testExecuteAndGetExpectedListOfModulesAndStatus(): void
    {
        // Arrange
        [$command, $commandTester] = $this->getCommandTester();
        $commandTester->execute([
            'command' => $command->getName(),
            // pass arguments to the helper
            // prefix the key with two dashes when passing options,
            // e.g: '--some-option' => 'option_value',
            '--path' => codecept_data_dir('app'),
        ]);

        $expected = '
Detect modules:
===============

Scan Module directory
---------------------

 0/6 [░░░░░░░░░░░░░░░░░░░░░░░░░░░░]   0%
 6/6 [▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓] 100%

 ------------- ----------------- ------------------ 
  ModuleName    Has Codeception   Has Bootstrapped  
 ------------- ----------------- ------------------ 
  admin         ok                ok                
  application   NO!               NO!               
  core          ok                ok                
  error         ok                ok                
  no-codecept   NO!               NO!               
  skip          NO!               NO!               
 ------------- ----------------- ------------------ 
';
        // Act
        // the output of the command in the console
        $output = $commandTester->getDisplay();

        // Assert
        $this->assertSame($expected, $output);
        $this->assertSame(0, $commandTester->getStatusCode());
    }

    public function testExecuteAndGetExpectedListOfModulesAndStatusWithNoProgressBar(): void
    {
        // Arrange
        [$command, $commandTester] = $this->getCommandTester();
        $commandTester->execute([
            'command' => $command->getName(),
            // pass arguments to the helper
            // prefix the key with two dashes when passing options,
            // e.g: '--some-option' => 'option_value',
            '--path' => codecept_data_dir('app'),
            '-s'     => '1',
        ]);

        $expected = '
Detect modules:
===============

Scan Module directory
---------------------

 ------------- ----------------- ------------------ 
  ModuleName    Has Codeception   Has Bootstrapped  
 ------------- ----------------- ------------------ 
  admin         ok                ok                
  application   NO!               NO!               
  core          ok                ok                
  error         ok                ok                
  no-codecept   NO!               NO!               
  skip          NO!               NO!               
 ------------- ----------------- ------------------ 
';
        // Act
        // the output of the command in the console
        $output = $commandTester->getDisplay();

        // Assert
        $this->assertSame($expected, $output);
        $this->assertSame(0, $commandTester->getStatusCode());
    }

    public function testExecuteAndGetExpectedListOfModulesWithFilteredResultAndStatus(): void
    {
        // Arrange
        [$command, $commandTester] = $this->getCommandTester();
        $commandTester->execute([
            'command' => $command->getName(),
            // pass arguments to the helper
            // prefix the key with two dashes when passing options,
            // e.g: '--some-option' => 'option_value',
            '--path'    => codecept_data_dir('app'),
            '--exclude' => ['skip', 'application', 'no-codecept'],
        ]);

        $expected = '
Detect modules:
===============

Scan Module directory
---------------------

 0/6 [░░░░░░░░░░░░░░░░░░░░░░░░░░░░]   0%
 6/6 [▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓] 100%

 ------------ ----------------- ------------------ 
  ModuleName   Has Codeception   Has Bootstrapped  
 ------------ ----------------- ------------------ 
  admin        ok                ok                
  core         ok                ok                
  error        ok                ok                
 ------------ ----------------- ------------------ 
';
        // Act
        // the output of the command in the console
        $output = $commandTester->getDisplay();

        // Assert
        $this->assertSame($expected, $output);
        $this->assertSame(0, $commandTester->getStatusCode());
    }

    public function testExecuteAndGetExpectedListOfModulesWithFilteredResultAndStatusAndNoProgressBar(): void
    {
        // Arrange
        [$command, $commandTester] = $this->getCommandTester();
        $commandTester->execute([
            'command' => $command->getName(),
            // pass arguments to the helper
            // prefix the key with two dashes when passing options,
            // e.g: '--some-option' => 'option_value',
            '--path'    => codecept_data_dir('app'),
            '--exclude' => ['skip', 'application', 'no-codecept'],
            '-s'        => 1,
        ]);

        $expected = '
Detect modules:
===============

Scan Module directory
---------------------

 ------------ ----------------- ------------------ 
  ModuleName   Has Codeception   Has Bootstrapped  
 ------------ ----------------- ------------------ 
  admin        ok                ok                
  core         ok                ok                
  error        ok                ok                
 ------------ ----------------- ------------------ 
';
        // Act
        // the output of the command in the console
        $output = $commandTester->getDisplay();

        // Assert
        $this->assertSame($expected, $output);
        $this->assertSame(0, $commandTester->getStatusCode());
    }

    /**
     * @return array
     */
    protected function getCommandTester(): array
    {
        $application = new Application();
        $application->add(new Detect());

        $command = $application->find('codeception:modules:detect');

        $commandTester = new CommandTester($command);

        return [$command, $commandTester];
    }
}
