<?php
declare(strict_types=1);

namespace Webproject\Console\Tests\Unit;

use Codeception\Test\Unit;
use Symfony\Component\Console\Application;
use Symfony\Component\Console\Tester\CommandTester;
use Webproject\Console\Command\Modules\Run;

use function codecept_data_dir;

/**
 * Class RunTest
 */
class RunTest extends Unit
{
    public function testExecuteAndGetExpectedOutputResultOfModulesAndStatus(): void
    {
        // Arrange
        [$command, $commandTester] = $this->getCommandTester();
        /** @var CommandTester $commandTester */
        $commandTester->execute(
            [
                'command' => $command->getName(),
                // pass arguments to the helper
                // prefix the key with two dashes when passing options,
                // e.g: '--some-option' => 'option_value',
                '--path' => codecept_data_dir('app'),
                '-s',
            ],
            [
                'interactive' => false,
            ]
        );

        $expected = '
Run Codeception for modules:
============================

Scan Module directory
---------------------
';
        // Act
        // the output of the command in the console
        $output = $commandTester->getDisplay();
        // remove dynamic time and memory values
        $output = \preg_replace('#\d* ms#', '99 ms', $output);
        $output = \preg_replace('#\d*\.\d{2} MB#', '1.00 MB', $output);
        // Assert
        $this->assertStringContainsString($expected, $output);
        $this->assertStringContainsString('Admin', $output);
        $this->assertStringContainsString('Application', $output);
        $this->assertStringContainsString('Core', $output);
        $this->assertStringContainsString('Error', $output);
        $this->assertStringContainsString('No-codecept', $output);
        $this->assertStringContainsString('Skip', $output);
        $this->assertSame(3, \substr_count($output, '99 ms'), '99 ms');
        $this->assertSame(3, \substr_count($output, '1.00 MB'), '1.00 MB');
        $this->assertSame(1, \substr_count($output, 'ok'), 'ok');
        $this->assertSame(5, \substr_count($output, 'error!'), 'error!');

        $this->assertSame(255, $commandTester->getStatusCode());
    }

    public function testExecuteAndGetExpectedOutputResultOfModulesAndStatusWithNoProgressBar(): void
    {
        // Arrange
        [$command, $commandTester] = $this->getCommandTester();
        $commandTester->execute(
            [
                'command' => $command->getName(),
                // pass arguments to the helper
                // prefix the key with two dashes when passing options,
                // e.g: '--some-option' => 'option_value',
                '--path' => codecept_data_dir('app'),
                '-s'     => '1',
            ],
            [
                'interactive' => false,
            ]
        );

        $expected = '
Run Codeception for modules:
============================

Scan Module directory
---------------------

Executing modules:
------------------

';
        // Act
        // the output of the command in the console
        $output = $commandTester->getDisplay();

        // remove dynamic time and memory values
        $output = \preg_replace('#\d* ms#', '99 ms', $output);
        $output = \preg_replace('#\d*\.\d{2} MB#', '1.00 MB', $output);

        // Assert
        $this->assertStringContainsString($expected, $output);
        $this->assertStringContainsString('Admin', $output);
        $this->assertStringContainsString('Application', $output);
        $this->assertStringContainsString('Core', $output);
        $this->assertStringContainsString('Error', $output);
        $this->assertStringContainsString('No-codecept', $output);
        $this->assertStringContainsString('Skip', $output);
        $this->assertSame(3, \substr_count($output, '99 ms'));
        $this->assertSame(3, \substr_count($output, '1.00 MB'));
        $this->assertSame(1, \substr_count($output, 'ok'));
        $this->assertSame(5, \substr_count($output, 'error!'));

        $this->assertSame(255, $commandTester->getStatusCode());
    }

    public function testExecuteAndGetExpectedOutputResultOfModulesWithFilteredResultAndStatus(): void
    {
        // Arrange
        [$command, $commandTester] = $this->getCommandTester();
        $commandTester->execute(
            [
                'command' => $command->getName(),
                // pass arguments to the helper
                // prefix the key with two dashes when passing options,
                // e.g: '--some-option' => 'option_value',
                '--path'    => codecept_data_dir('app'),
                '--exclude' => ['skip', 'application', 'no-codecept'],
                '-s',
            ],
            [
                'interactive' => false,
            ]
        );
        $expected = '
Run Codeception for modules:
============================

Scan Module directory
---------------------
';
        // Act
        // the output of the command in the console
        $output = $commandTester->getDisplay();

        // remove time and memory integers (dynamic)
        $output = \preg_replace('#\d* ms#', '99 ms', $output);
        $output = \preg_replace('#\d*\.\d{2} MB#', '1.00 MB', $output);

        // Assert
        $this->assertStringContainsString($expected, $output);
        $this->assertStringContainsString('Admin', $output);
        $this->assertStringContainsString('Core', $output);
        $this->assertStringContainsString('Error', $output);
        $this->assertSame(3, \substr_count($output, '99 ms'));
        $this->assertSame(3, \substr_count($output, '1.00 MB'));
        $this->assertSame(1, \substr_count($output, 'ok'));
        $this->assertSame(2, \substr_count($output, 'error!'));

        $this->assertSame(1, $commandTester->getStatusCode());
    }

    public function testExecuteAndGetExpectedOutputResultOfModulesWithFilteredResultAndStatusAndNoProgressBar(): void
    {
        // Arrange
        [$command, $commandTester] = $this->getCommandTester();
        $commandTester->execute([
            'command' => $command->getName(),
            // pass arguments to the helper
            // prefix the key with two dashes when passing options,
            // e.g: '--some-option' => 'option_value',
            '--path'    => codecept_data_dir('app'),
            '--exclude' => ['skip', 'application', 'no-codecept'],
            '-s'        => 1,
        ], [
            'interactive' => false,
        ]);

        $expected = '
Run Codeception for modules:
============================

Scan Module directory
---------------------

Executing modules:
------------------

';
        // Act
        // the output of the command in the console
        $output = $commandTester->getDisplay(true);
        // remove dynamic time and memory values
        $output = \preg_replace('#\d* ms#', '99 ms', $output);
        $output = \preg_replace('#\d*\.\d{2} MB#', '1.00 MB', $output);
        // Assert
        $this->assertStringContainsString($expected, $output);
        $this->assertStringContainsString($expected, $output);
        $this->assertStringContainsString('Admin', $output);
        $this->assertStringContainsString('Core', $output);
        $this->assertStringContainsString('Error', $output);
        $this->assertSame(3, \substr_count($output, '99 ms'), '99 ms');
        $this->assertSame(3, \substr_count($output, '1.00 MB'), '1.00 MB');
        $this->assertSame(1, \substr_count($output, 'ok'), 'ok');
        $this->assertSame(2, \substr_count($output, 'error!'), 'error!');

        $this->assertSame(1, $commandTester->getStatusCode());
    }

    public function testExecuteAndGetExpectedOutputResultOfModulesWithFilteredResultAndStatusAndNoProgressBarNoErrors(): void
    {
        // Arrange
        [$command, $commandTester] = $this->getCommandTester();
        $commandTester->execute([
            'command' => $command->getName(),
            // pass arguments to the helper
            // prefix the key with two dashes when passing options,
            // e.g: '--some-option' => 'option_value',
            '--path'    => codecept_data_dir('app'),
            '--exclude' => ['skip', 'application', 'no-codecept', 'error', 'admin'],
            '-s'        => 1,
        ], [
            'interactive' => false,
        ]);

        $expected = '
Run Codeception for modules:
============================

Scan Module directory
---------------------

Executing modules:
------------------

';
        // Act
        // the output of the command in the console
        $output = $commandTester->getDisplay(true);
        // remove dynamic time and memory values
        $output = \preg_replace('#\d* ms#', '99 ms', $output);
        $output = \preg_replace('#\d*\.\d{2} MB#', '1.00 MB', $output);
        // Assert
        $this->assertStringContainsString($expected, $output);
        $this->assertStringContainsString($expected, $output);
        $this->assertStringNotContainsString('Admin', $output);
        $this->assertStringContainsString('Core', $output);
        $this->assertStringContainsString('Error', $output);
        $this->assertSame(1, \substr_count($output, '99 ms'), '99 ms');
        $this->assertSame(1, \substr_count($output, '1.00 MB'), '1.00 MB');
        $this->assertSame(1, \substr_count($output, 'ok'), 'ok');
        $this->assertSame(0, \substr_count($output, 'error!'), 'error!');

        $this->assertSame(0, $commandTester->getStatusCode());
    }

    /**
     * @return array
     */
    protected function getCommandTester(): array
    {
        $application = new Application();
        $application->add(new Run());

        $command = $application->find('codeception:modules:run');

        $commandTester = new CommandTester($command);

        return [$command, $commandTester];
    }
}
