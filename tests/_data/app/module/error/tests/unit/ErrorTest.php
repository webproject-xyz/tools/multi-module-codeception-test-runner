<?php declare(strict_types=1);
class ErrorTest extends \Codeception\Test\Unit
{
    /**
     * @var \UnitTester
     */
    protected $tester;

    // tests
    public function testSomeFeature()
    {
        throw new ErrorException('error in module');
    }
}
