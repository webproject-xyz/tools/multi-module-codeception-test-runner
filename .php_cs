<?php
declare(strict_types=1);

$header = <<<'EOF'
/** @todo change me:
 * @link      http://github.com/zendframework/ZendSkeletonApplication for the canonical source repository
 *
 * @copyright Copyright (c) 2005-2016 Zend Technologies USA Inc. (http://www.zend.com)
 * @license   http://framework.zend.com/license/new-bsd New BSD License
 */
EOF;

$finder = PhpCsFixer\Finder::create()
   ->ignoreDotFiles(true)
   ->in(__DIR__ .'/')
   ->in(__DIR__ .'/src')
   ->in(__DIR__ .'/tests')
   ->notPath('/_generated/')
    ->name(['*.php'])
;

return PhpCsFixer\Config::create()
    ->setRiskyAllowed(true)
    ->setRules([
        '@PHP56Migration' => true,
        'encoding' => true,
        'single_blank_line_at_eof' => true,
        'no_spaces_after_function_name' => true,
        'no_closing_tag' => true,
        'object_operator_without_whitespace' => true,
        'single_import_per_statement' => true,
        'method_argument_space' => true,
        'line_ending' => true,
        'no_alias_functions' => true,
        'no_empty_statement' => true,
        'indentation_type' => true,
        'blank_line_after_namespace' => true,
        'lowercase_keywords' => true,
        'no_spaces_inside_parenthesis' => true,
        'braces' => true,
        'no_trailing_whitespace' => true,
        'no_unused_imports' => true,
        'no_whitespace_in_blank_line' => true,
        'visibility_required' => true,
        'standardize_not_equals' => true,
        'full_opening_tag' => true,
        'array_syntax' => ['syntax' => 'short'],
        'combine_consecutive_unsets' => true,
        // one should use PHPUnit methods to set up expected exception instead of annotations
        'general_phpdoc_annotation_remove' => ['expectedException', 'expectedExceptionMessage', 'expectedExceptionMessageRegExp'],
        // 'header_comment' => ['header' => $header],
        'heredoc_to_nowdoc' => true,
        'list_syntax' => ['syntax' => 'short'],
        'no_extra_consecutive_blank_lines' => [
            'break',
            'continue',
            'extra',
            'return',
            'throw',
            'parenthesis_brace_block',
            'square_brace_block',
            'curly_brace_block'
        ],
        'no_short_echo_tag' => true,
        'no_unreachable_default_argument_value' => true,
        'no_useless_else' => true,
        'no_useless_return' => true,
        'ordered_class_elements' => false,
        //ordered_class_elements
        //          Orders the elements of classes/interfaces/traits.
        //          Configuration options:
        //          order (a subset of ['use_trait', 'public', 'protected', 'private', 'constant', 'constant_public', 'constant_protected', 'constant_private', 'property', 'property_static', 'property_public', 'property_protected', 'property_private', 'property_public_static', 'property_protected_static', 'property_private_static', 'method', 'method_static', 'method_public', 'method_protected', 'method_private', 'method_public_static', 'method_protected_static', 'method_private_static', 'construct', 'destruct', 'magic', 'phpunit']): list of strings defining order of elements; defaults to ['use_trait', 'constant_public', 'constant_protected', 'constant_private', 'property_public', 'property_protected', 'property_private', 'construct', 'destruct', 'magic', 'phpunit', 'method_public', 'method_protected', 'method_private']
        //          sortAlgorithm ('alpha', 'none'): how multiple occurrences of same type statements should be sorted; defaults to 'none'
        'ordered_imports' => false,
        'php_unit_strict' => true,
        'php_unit_test_class_requires_covers' => false,
        'phpdoc_align' => true,
        'phpdoc_indent' => true,
        'phpdoc_no_access' => true,
        'phpdoc_no_empty_return' => true,
        'phpdoc_no_package' => false,
        'phpdoc_scalar' => true,
        'phpdoc_separation' => true,
        'phpdoc_to_comment' => false,
        'phpdoc_trim' => true,
        'phpdoc_types' => true,
        'phpdoc_var_without_name' => true,
        'phpdoc_add_missing_param_annotation' => true,
        'phpdoc_order' => true,
        'phpdoc_trim_consecutive_blank_line_separation' => true,
        'phpdoc_types_order' => true,
        'semicolon_after_instruction' => true,
        'strict_comparison' => true,
        'ternary_to_null_coalescing' => true,
        'strict_param' => true,
        'yoda_style' => true,
        'multiline_comment_opening_closing' => false,
        'native_function_invocation' => true,
        'native_constant_invocation' => true,
        'native_function_casing' => true,
        '@PHP71Migration' => true,
        'function_typehint_space' => false,
        'fully_qualified_strict_types' => true,
        'modernize_types_casting' => true,
        'return_type_declaration' => ['space_before' => 'none'],

        'single_blank_line_before_namespace' => true,
        'short_scalar_cast' => true,
        'declare_strict_types' => true,
        'dir_constant' => true,
    ])
    ->setFinder(
        $finder
    )
;
