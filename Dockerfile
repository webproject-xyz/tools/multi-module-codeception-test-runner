FROM php:7-alpine
MAINTAINER Benjamin Fahl <git@webproject.xyz>

ENV COMPOSER_HOME /composer
ENV COMPOSER_ALLOW_SUPERUSER 1
ENV PATH /composer/vendor/bin:$PATH
ARG APP_VERSION

RUN apk --update --no-cache add \
    php7 \
    php7-ctype \
    php7-curl \
    php7-dom \
    php7-fileinfo \
    php7-ftp \
    php7-iconv \
    php7-json \
    php7-mbstring \
    php7-mysqlnd \
    php7-openssl \
    php7-pdo \
    php7-pdo_sqlite \
    php7-phar \
    php7-posix \
    php7-session \
    php7-simplexml \
    php7-sqlite3 \
    php7-tokenizer \
    php7-xml \
    php7-xmlreader \
    php7-xmlwriter \
    php7-zlib \
    php7-zip \
    git \
    && echo "memory_limit=-1" > /etc/php7/conf.d/99_memory-limit.ini \
    && rm -rf /var/cache/apk/* /var/tmp/* /tmp/*

COPY --from=composer:latest /usr/bin/composer /usr/bin/composer

RUN composer global config minimum-stability "dev" \
    && composer global config prefer-stable true \
    && composer global require hirak/prestissimo \
    && composer global require --no-suggest --no-progress -o webproject-xyz/codeception-multi-module-test-runner "$APP_VERSION"

VOLUME ["/app"]
WORKDIR /app

ENTRYPOINT ["module-runner"]
